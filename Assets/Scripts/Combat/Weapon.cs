﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TypeOfDamage {

	Perfurating,
	Cutting,
	Bruising,

	Ranged,
	Magical,
	Special,
}

public enum ElementalDamage {

	Neutral,

	Water,
	Earth,
	Fire,
	Air,

	Other,
}

public class Weapon : MonoBehaviour {

	


}
