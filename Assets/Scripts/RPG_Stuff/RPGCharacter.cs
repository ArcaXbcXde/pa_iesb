﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RPGCharacter : ComplexLivingBeing {
    
	public string charName;

	public float height = 1.5f, weight = 60.0f;

	[Range (1, 99)]
	public int strength = 1, intelligence = 1, dexterity = 1, constitution = 1, charisma = 1, wisdom = 1;
}
