﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CharacterClass {

	Classless,
	FilthyPeasant,
	MarvelousNobel,

	Warrior,
	Scout,
	Wizard,

	Mage,
	Archmage,
	Sorcerer,
	Necromancer,
	Rogue,
	Thief,
	Assassin,
	Barbarian,
	Monk,
	Ranger,
	Paladin,
	Cleric,
	Clown,
	King,
	Queen,
	Prince,
	Princess
}

public enum CharacterRace {

	Human,
	Orc,
	Elf,
	Dwarf,
	Gnome,
	Gremlin,
	Halfling,
	Fairy,
	Dragonborn,
	Unknown
}

public enum CharacterSkill {

	Jump,
	Kill,
	Die,
	Hit,
	BarrelRoll
}

public class RPGHero : RPGCharacter {

	public CharacterClass heroClass;
	public CharacterRace heroRace;
	public CharacterSkill[] heroSkill;

	
}
