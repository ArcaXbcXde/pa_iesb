﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LivingBeing : MonoBehaviour {

	protected int HPMax;
	public int HP = 10;

	protected void Awake () {
		
		HPMax = HP;
	}

	public void TakeDamage (int damage, float critChance) {
		
		if (HP > 0) {

			HP = HP - damage;
		}
		else if (HP <= 0) {
			Death();
		}

	}


	public void TakeDamage (int damage, float critChance, TypeOfDamage damageType) {


	}

	public void TakeDamage (int damage, float critChance, ElementalDamage damageElement) {


	}

	public void TakeDamage (int damage, float critChance, TypeOfDamage damageType, ElementalDamage damageElement) {

	}



	public void Death () {


	}

}
